<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('assembly_id')->unsigned()->nullable();
            //$table->foreign('assembly_id')->references('id')->on('assemblies')->onDelete('restrict')->onUpdate('cascade');
            $table->string('user_id',20)->nullable();
            $table->string('password')->nullable();
            $table->string('firstname',30)->nullable();
            $table->string('middlename',50)->nullable();
            $table->string('lastname',15)->nullable();
            $table->string('nationality')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('region_of_birth',20)->nullable();
            $table->string('hometown',30)->nullable();
            $table->string('language',30)->nullable();
            $table->string('occupation',30)->nullable();
            $table->string('title',30)->nullable();
            $table->char('gender', 1);
            $table->date('dob')->nullable();
            $table->date('date_join',11)->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('postal_add',60)->nullable();
            $table->string('digi_add',15)->nullable();
            $table->string('residential_add',50)->nullable();
            $table->string('emerg_name',25)->nullable();
            $table->string('emerg_phone')->nullable();
            $table->string('emerg_email')->nullable();
            $table->string('baptism',5)->nullable();
            $table->date('baptism_date')->nullable();
            $table->string('holy_baptism',5)->nullable();
            $table->date('holy_baptism_date')->nullable();
            $table->string('mar_status')->nullable();
            $table->string('qualification')->nullable();
            $table->string('other_qualification')->nullable();
            $table->string('profession',20)->nullable();
            $table->string('image',60)->nullable();
            $table->string('account_status')->nullable();
            $table->string('extra_info',255)->nullable();
















            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
