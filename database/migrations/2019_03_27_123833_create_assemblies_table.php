<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssembliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assemblies', function (Blueprint $table) {
            $table->string('id');
            $table->integer('areas_id')->unsigned()->nullable();
            //$table->foreign('areas_id')->references('id')->on('Areas')->onDelete('restrict')->onUpdate('cascade');
            $table->integer('minister_id')->unsigned()->nullable();
            //$table->foreign('minister_id',55)->references('id')->on('Ministers')->onDelete('restrict')->onUpdate('cascade');
            $table->string('name',30)->nullable();
            $table->string('location',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('tel')->nullable();
            $table->string('front_pic')->nullable();
            $table->string('back_pic')->nullable();
            $table->string('chu_building',255)->nullable();
            $table->string('mission_hse',255)->nullable();
            $table->string('extra_info',255)->nullable();
            $table->string('postal_add',60)->nullable();
            $table->date('date_founded')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assemblies');
    }
}
