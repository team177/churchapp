<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->string('id');
            $table->integer('territories_id')->unsigned()->nullable();
           // $table->foreign('territories_id')->references('id')->on('Territories')->onDelete('restrict')->onUpdate('cascade');
            $table->integer('minister_id')->unsigned()->nullable();
           // $table->foreign('minister_id',55)->references('id')->on('Ministers')->onDelete('restrict')->onUpdate('cascade');
            $table->string('name',30)->nullable();
            $table->string('location',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('tel')->nullable();
            $table->string('image')->nullable();
            $table->string('extra_info',255)->nullable();
            $table->string('postal_add',60)->nullable();
            $table->date('date_founded')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
