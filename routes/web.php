<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

#####################################################################################################
#######                                 Index 
#####################################################################################################

Route::get('/login', 'IndexController@login');
Route::post('/login', 'IndexController@loginUser');
Route::get('/register','IndexController@register');
Route::get('/forgotpassword','IndexController@resetPass');
//Route::get('/memprofile','IndexController@memProfile');
Route::post('/registeruser','IndexController@registerUser');

#####################################################################################################
#######                                 Profile 
#####################################################################################################

Route::get('/create_memberprofile','ProfileController@memberPro');
Route::get('/editprofile','ProfileController@editProfile');

#####################################################################################################
#######                                 Member
#####################################################################################################

Route::get('/addnewmember','AssemblyController\MemberController@addNewMember');
Route::get('/memberlist','AssemblyController\MemberController@memberList');
Route::get('/member/profile/{id}','AssemblyController\MemberController@profile');
Route::post('/savememberinformation','AssemblyController\MemberController@saveMemberInformation');
Route::get('/deletemember','AssemblyController\MemberController@deleteMember');
//Route::get('/generateusername','AssemblyController\MemberController@generateUsername');
Route::get('/editmember/{id}','AssemblyController\MemberController@editMember');
Route::post('/savemembereditinformation','AssemblyController\MemberController@saveMemberEditInformation');
#####################################################################################################
#######                                 Group
#####################################################################################################
Route::get('/addgroup','AssemblyController\BatchController@addgroup');
Route::post('/savegroup','AssemblyController\BatchController@savegroup');
Route::get('/grouplist','AssemblyController\BatchController@groupList');
Route::get('/deletegroup','AssemblyController\BatchController@deleteGroup');
Route::get('/deleteuserfromgroup','AssemblyController\BatchController@deleteUserFromGroup');
Route::get('/editgroup/{id}','AssemblyController\BatchController@editGroup');
Route::post('/saveeditgroup','AssemblyController\BatchController@saveEditGroup');
Route::post('/sendmessagetousers','AssemblyController\BatchController@sendSmsToUsers');

#####################################################################################################
#######                                 Church 
#####################################################################################################

Route::get('/churchlist','ChurchController@showChurchList');
Route::get('/viewnationalsum','ChurchController@viewNationalSum');
Route::get('/viewterritorysum','ChurchController@viewTerritorySum');
Route::get('/viewareasum','ChurchController@viewAreaSum');
Route::get('/viewAssemblysum','ChurchController@viewAssemblySum');
Route::get('/addFamilygroup','ChurchController@addFamilyGroup');
Route::get('/viewFamilygroup','ChurchController@viewFamilyGroup');

#####################################################################################################
#######                                 Requests
#####################################################################################################
Route::get('/processintransferreq','RequestController@processInTransferReq');
Route::get('/processouttransferreq','RequestController@processOutTransferReq');
Route::get('/newtransferreq','RequestController@NewTransferReq');
Route::get('/showtransferreq','RequestController@ShowTransferReq');
Route::get('/viewtransferreq','RequestController@viewTransferReq');
Route::get('/processburialreq','RequestController@processBurialReq');
Route::get('/newburialreq','RequestController@newBurialReq');
Route::get('/viewburialreq','RequestController@viewBurialReq');


#####################################################################################################
#######                                 Church
##################################################################################################### 
Route::get('/addnewchurch','AssemblyController\churchController@addNewchurch');
Route::post('/addnewchurch','AssemblyController\churchController@savechurchDetails');
Route::get('/churchlist','AssemblyController\churchController@churchList');
Route::get('/churchdetails/{id}','AssemblyController\churchController@churchDetails');
Route::get('/deletechurch','AssemblyController\churchController@deletechurch');
Route::get('/editchurch/{id}','AssemblyController\churchController@editchurch');
Route::post('/saveeditchurch','AssemblyController\churchController@saveEditchurch');

#####################################################################################################
#######                                 Staff
#####################################################################################################
Route::get('/addnewstaff','AssemblyController\StaffController@addNewStaff');
Route::post('/addnewstaff','AssemblyController\StaffController@saveStaffDetails');
Route::get('/stafflist','AssemblyController\StaffController@staffList');
Route::get('/staff/profile/{id}','AssemblyController\StaffController@profile');
Route::get('/deletestaff','AssemblyController\StaffController@deleteStaff');
Route::get('/generateusernameforstaff','AssemblyController\StaffController@generateUsernameForStaff');
Route::get('/generateusernameforstaffwithalias','AssemblyController\StaffController@generateUsernameForStaffWithAlias');
Route::get('/editstaff/{id}','AssemblyController\StaffController@editStaff');
Route::post('/updatestaff','AssemblyController\StaffController@updateStaff');

#####################################################################################################
#######                                 Staff Category
#####################################################################################################
Route::get('/staffcategory','AssemblyController\StaffCategoryController@categoryList');
Route::post('/savestaffcategory','AssemblyController\StaffCategoryController@saveStaffCategory');
Route::get('/deletestaffcategory','AssemblyController\StaffCategoryController@deleteStaffCategory');
Route::post('/saveeditedstaffcategory','AssemblyController\StaffCategoryController@saveEditedStaffCategory');

#####################################################################################################
#######                                 Calendar
#####################################################################################################
Route::get('/calender','AssemblyController\CalenderController@index');
Route::get('/fetchcalendardata','AssemblyController\CalenderController@fetchCalendarData');


#####################################################################################################
#######                                 News
#####################################################################################################
Route::get('/postnews','AssemblyController\NewsController@postnews');
Route::post('/savenews','AssemblyController\NewsController@saveNews');
Route::get('/deletenews','AssemblyController\NewsController@deleteNews');
Route::get('/editnews/{id}','AssemblyController\NewsController@editNews');
Route::post('/saveeditnews','AssemblyController\NewsController@saveEditNews');
Route::get('/newslist','AssemblyController\NewsController@newsList');

#####################################################################################################
#######                                 Events
#####################################################################################################
Route::get('/postevents','AssemblyController\EventsController@postAnEvent');
Route::post('/saveevent','AssemblyController\EventsController@saveEvent');
Route::get('/eventlist','AssemblyController\EventsController@eventList');
Route::get('/deleteevent','AssemblyController\EventsController@deleteEvent');
Route::post('/saveeditevent','AssemblyController\EventsController@saveEditEvent');
Route::get('/editevent/{id}','AssemblyController\EventsController@editEvent');
Route::post('/saveeventfromcalender','AssemblyController\EventsController@saveeventfromcalender');
Route::get('/pullthisevent','AssemblyController\EventsController@pullthisevent');
Route::post('/saveediteventfromcalender','AssemblyController\EventsController@saveediteventfromcalender');

#####################################################################################################
#######                                 Buses
#####################################################################################################
Route::post('/savebusdetails','AssemblyController\BusController@saveBusDetails');
Route::get('/deletebusdetails','AssemblyController\BusController@deleteBusDetails');
Route::get('/manageroute','AssemblyController\BusController@manageRoute');
Route::post('/savebusroute','AssemblyController\BusController@savebusroute');
Route::post('/saveeditbusdetails','AssemblyController\BusController@saveeditbusdetails');
Route::get('/deleteroutedetails','AssemblyController\BusController@deleteroutedetails');
Route::post('/saveeditbusroute','AssemblyController\BusController@saveeditbusroute');


#####################################################################################################
#######                                 Announcements
#####################################################################################################
 Route::post('/saveannouncement','AssemblyController\AnnouncementController@saveAnnouncement');
 Route::get('/deleteannouncement','AssemblyController\AnnouncementController@deleteAnnouncement');
 Route::get('/announcementlist','AssemblyController\AnnouncementController@announcementList');
 Route::get('/editannouncement/{id}','AssemblyController\AnnouncementController@editAnnouncement');
 Route::post('/saveeditannouncement','AssemblyController\AnnouncementController@saveEditAnnouncement');





