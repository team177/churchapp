@extends('layouts.partials.mainlayout')

@section('css-above')
<link rel="stylesheet" href="{{asset('/assets/jquery.steps/jquery.steps.css')}}">

<link rel="stylesheet" href="{{asset('/assets/jquery.steps/jquery.steps.css')}}">

<link rel="stylesheet" href="{{asset('/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('content')


<form action="{{url('/savememberinformation')}}" method="post" class="icons-tab-steps wizard-circle" id="example-advanced-form">    <h3>Basic Profile</h3>
    <fieldset>
        <legend>Primary Information</legend>
 
                <div class="row">

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="firstname">First Name *:</label>

                                            <input type="text" class="form-control" id="firstname" name="firstname" required="required">

                                        </div>

                                    </div>

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="middlename">Middle Name :</label>

                                            <input type="text" class="form-control" id="middlename" name="middlename">

                                        </div>

                                    </div>



                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="lastname">Last Name *:</label>

                                            <input type="text" class="form-control" id="lastname" name="lastname" required="required">

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="gender">Gender *:</label>

                                    <select class="form-control" id="gender" name="gender" required="required">

                                        <option value="">Select Gender</option>

                                        <option value="M">Male</option>

                                        <option value="F">Female</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="dob">Date of Birth :</label>

                                    <input type="text" class="form-control pickadate" id="dob" name="dob">

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="place_of_birth">Place of Birth :</label>

                                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" placeholder="Accra">

                                </div>

                            </div>

                            </div>

                            <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="nationality">Nationality :</label>

                                    <select class="form-control" id="nationality" name="nationality">

                                        <option value="Ghanaian">Ghanaian</option>

                                        <option value="Non-Ghanaian">Non-Ghanaian</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="nationality">Residential Address :</label>

                                    <input type="text" class="form-control" id="residence" name="residence" placeholder="Madina New Road 589/3">

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="phone">Telephone :</label>

                                    <input type="text" class="form-control" id="phone" maxlength="10" name="phone" placeholder="0201234567" required="required">

                                </div>

                            </div>

                            </div>

                            <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="member_username">Username *:</label>

                                <input type="text" class="form-control" id="member_username" name="member_username" required="required">

                            </div>

                        </div>



                        <div class="col-md-6">

                            <div class="form-group">

                                <label for="member_password">Password *:</label>

                                <input type="text" class="form-control" id="member_password" name="member_password" required="required" placeholder="Must be above 8 characters">

                            </div>

                        </div>

                        </div>
                    <div class="row">

                    <div class="col-md-4">

                        <div class="form-group">

                            <label for="email_add">Email Address*:</label>

                            <input type="email" class="form-control" id="email_add" name="email_add" placeholder="daniel@example.com">

                        </div>

                    </div>


                    <div class="col-md-4">

                        <div class="form-group">

                            <label for="post_add"> Postal Address:</label>

                            <input type="text" class="form-control pickadate" id="post_add" name="post_add" placeholder="P.O.Box 1000">

                        </div>

                    </div>


                    </div>
                    <div class="row">

                    <div class="col-md-4">

                        <div class="form-group">

                            <label for="language">Language(s) :</label>

                            <input type="text" class="form-control " id="language" name="language" placeholder="Twi, Ga ">

                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="form-group">

                            <label for="hometown">Hometown *:</label>

                            <input type="text" class="form-control" id="hometown" name="hometown" placeholder="Larteh">

                        </div>

                    </div>



                    <div class="col-md-4">

                        <div class="form-group">

                            <label for="home_reg"> Home Region:</label>

                            <input type="text" class="form-control" id="home_reg" name="home_reg" placeholder="Eastern Region">

                        </div>

                    </div>

                    </div>
                    <div class="row">
                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="gender">Marital Status *</label>
                                            <select class="form-control" id="marital_status" name="marital_status" required="required">
                                                <option value="">Select Status</option>
                                                <option value="S">Single</option>
                                                <option value="M">Married</option>
                                            </select>
                                        </div>
                                    </div>
</div>


                    <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="spouse_name">Spouse Name</label>
                                            <input type="text" id="spouse_name" class="form-control" placeholder="Spouse Name" name="spouse_name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="spouse_contact">Spouse Contact</label>
                                            <input type="text" id="spouse_contact" maxlength="10" class="form-control" placeholder="Spouse Contact" name="spouse_contact">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="spouse_email">Spouse Email</label>
                                            <input type="text" id="spouse_email" class="form-control" placeholder="Spouse Email" name="spouse_email">
                                        </div>
                                    </div>
                                </div>
                    
    </fieldset>
 
    <h3>Church Information</h3>
    <fieldset>

        <legend> Information</legend>
 
                             <div class="row">

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="loc_church">Local Assembly *:</label>

                                            <input type="text" class="form-control" id="loc_church" name="loc_church" required="required">

                                        </div>

                                    </div>

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="for_church">Former Denomination/Assembly :</label>

                                            <input type="text" class="form-control" id="for_church" name="for_church">

                                        </div>

                                    </div>



                                    <div class="col-md-4">

                                <div class="form-group">

                                    <label for="date_join">Date Joined CACI :</label>

                                    <input type="date" class="form-control pickadate" id="date_join" name="date_join">

                                </div>

                            </div>

                                </div>

                                <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">

                                    <label for="babtised">Baptised ? :</label>

                                    <select class="form-control" id="baptised" name="baptised" >

                                        <option value="">Select</option>

                                        <option value="Y">Yes</option>

                                        <option value="N">No</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-6">

                                <div class="form-group">

                                    <label for="do_bap">Date of Baptism :</label>

                                    <input type="date" class="form-control pickadate" id="do_bap" name="do_bap">

                                </div>

                            </div>


                            </div>

                            <div class="row">

                            <div class="col-md-6">

                                <div class="form-group">

                                    <label for="ho_baptised">Holy Spirit Baptised ? :</label>

                                    <select class="form-control" id="ho_baptised" name="ho_baptised" >

                                        <option value="">Select</option>

                                        <option value="Y">Yes</option>

                                        <option value="N">No</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-6">

                                <div class="form-group">

                                    <label for="ho_bap">Date of Holy Spirit Baptism :</label>

                                    <input type="date" class="form-control pickadate" id="ho_bap" name="ho_bap">

                                </div>

                            </div>


                            </div>

                        
                                
    </fieldset>
 
    <h3>Educational Information</h3>
    <fieldset>
        <legend></legend>
                                        <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label for="edu_level">Highest Educational Level *:</label>

                                        <input type="text" class="form-control" id="edu_level" name="edu_level">

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label for="qual"> Other Qualification & Year of Award :</label>

                                        <input type="text" class="form-control" id="qual" name="qual">

                                    </div>

                                </div>

                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="extra">Extra Info</label>
                                    <textarea id="extra_info" rows="5" class="form-control" name="extra_info" placeholder="Other Information"></textarea>
                                </div>
                                </div>
                                </div>
                                


                                </div>
    </fieldset>
 
    <h3>Finish</h3>
    <fieldset>
        <legend>Terms and Conditions</legend>
 
        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="form-control"> <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
    </fieldset>
</form>

@endsection
@section('custom_scripts')
<script src="{{asset('assets/jquery.steps/jquery.steps.js')}}"></script>
<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('assets/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

<script>
var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        return true;
        // Allways allow previous action even if the current form is not valid!
        // if (currentIndex > newIndex)
        // {
        //     return true;
        // }
        // // Forbid next action on "Warning" step if the user is to young
        // if (newIndex === 3 && Number($("#age-2").val()) < 18)
        // {
        //     return false;
        // }
        // // Needed in some cases if the user went back (clean up)
        // if (currentIndex < newIndex)
        // {
        //     // To remove error styles
        //     form.find(".body:eq(" + newIndex + ") label.error").remove();
        //     form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        // }
        // form.validate().settings.ignore = ":enabled,:hidden";
        // return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
    },
    onFinishing: function (event, currentIndex)
    {
        // form.validate().settings.ignore = ":disabled";
        // return form.valid();
        return true;
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
})
$('a[href="#next"]').addClass('btn btn-block btn-success btn-lg');

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker, .pickadate').datepicker({
      autoclose: true
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })

</script>

<script type="text/javascript">

	 var loadFile = function(event) {

	 	console.log(event);

    var output = document.getElementById('output');

    output.src = URL.createObjectURL(event.target.files[0]);

     };

</script>
@endsection