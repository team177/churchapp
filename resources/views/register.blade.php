<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Christ Apostolic Church International</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('assets/plugins/iCheck/square/blue.css')}}">
<style>

  .btn-primary {
    background-color: #3c8dbc;
    border-color: #367fa9;
}
  .login-box, .register-box {
    width: 600px;
    margin: 7% auto;

}

.login-page, .register-page {
    /* background: #fff; */
    /* background: url('{{ asset("img/cry.jpg") }}') no-repeat center fixed !important; */
    background-size: cover !important;
    background-image: linear-gradient(to right,#73C2FB, #0F52BA);

}
.login-logo a, .register-logo a {
    color: #fff;
    text-decoration-style: wavy;

}
.register-box-body {
    background: #fff;
    padding: 20px;
    border-top: 5;
    color: #000;
    border-radius: 45px;
    opacity: 1;
    filter: alpha(opacity=50);
}
html, body {
    height: fit-content;
}

.form-control {
    border-radius: 10px;
    box-shadow: 1px 1px #000;
    border-color: #1b4394;
}



</style>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="{{asset('assets')}}/index2.html"><b>Christ Apostolic Church International</a>
  </div>

  <div class="register-box-body">
    <h3 class="login-box-msg">Register a new membership</h3>

    <form action="{{url('/registeruser')}}" method="post" id="registerforms" >
    {{ csrf_field() }}
    
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="firstname" placeholder="Firstname" required="required">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="lastname" placeholder="Surname" required="required">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email" required="required">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="number" class="form-control" name="phone" placeholder="Contact number" required="required">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback"> 
        <input type="password" class="form-control" name="password" placeholder="Password" required="required">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="confirmpassword" placeholder="Retype password" required="required">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" required="required"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
     
    </div>
<h4><u>
    <a href="{{url('/login')}}" class="text-center">I already have a membership</a></u></h4>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

<!-- <script>
$('#registerform').submit(function(e){
    e.preventDefault();
    $.post("{{url('/registeruser')}}",$(this).serialize(),function(res){
      if(res && res.status==='success'){
        
      }
    }).fail(function(){
      //alert the person if no internet
    });

})
</script> -->
</body>
</html>
