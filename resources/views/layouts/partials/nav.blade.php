<!-- <aside class="main-sidebar" style="background-color:#f56954;"> -->
<aside class="main-sidebar" style="background-color:#F50057;">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Daniel Adjei </p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard </a></li>
          </ul>
        <!-- <li><a href="{{url('/create_memberprofile')}}"><i class="fa fa-circle-o text-red"></i> <span>Create Profile</span></a></li>
        <li><a href="{{url('/editprofile')}}"><i class="fa fa-circle-o text-red"></i> <span>Edit Profile</span></a></li> -->

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/create_memberprofile')}}"><i class="fa fa-circle-o"></i> Create Profile</a></li>
            <li><a href="{{url('/editprofile')}}"><i class="fa fa-circle-o"></i> View Profile</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Churches</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/#')}}"><i class="fa fa-circle-o"></i> List</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Church Statistics</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="."><i class="fa fa-circle-o"></i> View National Summaries</a></li>
            <li><a href="."><i class="fa fa-circle-o"></i> View Territory Summaries</a></li>
            <li><a href="."><i class="fa fa-circle-o"></i> View Area Summaries</a></li>


          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Circuits</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> View Circuits</a></li>
            <li class="treeview">
              <!-- <a href="#"><i class="fa fa-circle-o"></i> Edit Circuits</a> -->
              </ul>
            </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Family Groups</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Family Group  </a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>View Family Groups </a></li>
            
            
              </ul>
            </li>
          </ul>
          </li>


          <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Requests </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Transfers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Process Incoming Requests </a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Process Outgoing Requests</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> My New Requests</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> View Requests</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> History</a></li>


            <li class="treeview">
              <!-- <a href="#"><i class="fa fa-circle-o"></i> Edit Circuits</a> -->
            </ul>
            </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Burial</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Process Burial Requests </a></li>
            <li class="treeview">
            <li><a href="#"><i class="fa fa-circle-o"></i> My New Requests</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> History</a></li>
              </ul>
            </li>
          </ul>
          </li>


          <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Departed</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Newly Departed</a></li>
            <!-- <li><a href="profile.html"><i class="fa fa-circle-o"></i> Edit Members</a></li> -->
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Departed</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Members</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add Members</a></li>
            <!-- <li><a href="profile.html"><i class="fa fa-circle-o"></i> Edit Members</a></li> -->
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Members</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Staff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New</a></li>
            <!-- <li><a href="profile.html"><i class="fa fa-circle-o"></i> Edit Members</a></li> -->
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Staff</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> Category</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Sermons</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New Sermon</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Sermons</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Drop Box</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Upload Documents</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Documents</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Transportation </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Vehicle Details</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> Manage Routes </a></li>
          </ul>
        </li>
      

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Visitors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add Visitors</a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> View Visitors</a></li>
          </ul>
        </li>
        

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Meetings </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="profile.html"><i class="fa fa-circle-o"></i> List</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Phone Book</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New</a></li>
            <li><a href="profile.html"><i class="fa fa-circle-o"></i> List Members</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Bulk SMS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Send Message </a></li>
            <li><a href="profile.html"><i class="fa fa-circle-o"></i> History </a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Annoucements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Send Announcements</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Appointments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            

          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Ministerial Appointments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> New Appointments </a></li>
            <li class="treeview">
              <li><a href="#"><i class="fa fa-circle-o"></i> View Appointments </a></li>
             

              </ul>
            </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Lay Appointments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> New Appointments </a></li>
            <li class="treeview">
              <li><a href="#"><i class="fa fa-circle-o"></i> View Appointments </a></li>
              </ul>
            </li>
          </ul>
          </li>


          <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Access Levels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Edit Access Levels </a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> Change Portal </a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Imprest Form </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New </a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> List </a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>  Retrieve Lost ID Cards</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Minister ID Card</a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Announcements </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> View Annoucements </a></li>
          </ul>
        </li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span> Gallery </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="invoice.html"><i class="fa fa-circle-o"></i> Add New </a></li>
            <li><a href="login.html"><i class="fa fa-circle-o"></i> List </a></li>
          </ul>
        </li>

        
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Calendar</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Statistics</span></a></li>


        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Official Status</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          </li>
            
          <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Ministerial Status</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Add New Status </a></li>
            <li class="treeview">
              <li><a href="#"><i class="fa fa-circle-o"></i> View Status </a></li>
              </ul>
            </li>


       

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>