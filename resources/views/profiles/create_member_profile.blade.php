@extends('layouts.partials.mainlayout')

@section('css-above')
<link rel="stylesheet" href="{{asset('/assets/jquery.steps/jquery.steps.css')}}">
@endsection

@section('content')


<form id="example-advanced-form" action="#">
    <h3>Basic Profile</h3>
    <fieldset>
        <legend>Primary Information</legend>
 
        <div class="row">

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="firstname">First Name *:</label>

                                            <input type="text" class="form-control" id="firstname" name="firstname" required="required">

                                        </div>

                                    </div>

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="middlename">Middle Name :</label>

                                            <input type="text" class="form-control" id="middlename" name="middlename">

                                        </div>

                                    </div>



                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="lastname">Last Name *:</label>

                                            <input type="text" class="form-control" id="lastname" name="lastname" required="required">

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="gender">Gender *:</label>

                                    <select class="form-control" id="gender" name="gender" required="required">

                                        <option value="">Select Gender</option>

                                        <option value="M">Male</option>

                                        <option value="F">Female</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="dob">Date of Birth :</label>

                                    <input type="text" class="form-control pickadate" id="dob" name="dob">

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="place_of_birth">Place of Birth :</label>

                                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" placeholder="Accra">

                                </div>

                            </div>

                            </div>

                            <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="nationality">Nationality :</label>

                                    <select class="form-control" id="nationality" name="nationality">

                                        <option value="Ghanaian">Ghanaian</option>

                                        <option value="Non-Ghanaian">Non-Ghanaian</option>

                                    </select>

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="nationality">Residential Address :</label>

                                    <input type="text" class="form-control" id="residence" name="residence" placeholder="Madina New Road 589/3">

                                </div>

                            </div>



                            <div class="col-md-4">

                                <div class="form-group">

                                    <label for="phone">Telephone :</label>

                                    <input type="text" class="form-control" id="phone" maxlength="10" name="phone" placeholder="0201234567">

                                </div>

                            </div>

                            </div>

                            <div class="row">

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="reg_date">Registration Date :</label>

                                <input type="text" class="form-control pickadate" id="reg_date" name="reg_date" placeholder="Date of Sign-up">

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="member_username">Username *:</label>

                                <input type="text" class="form-control" id="member_username" name="member_username" required="required">

                            </div>

                        </div>



                        <div class="col-md-4">

                            <div class="form-group">

                                <label for="member_password">Password *:</label>

                                <input type="text" class="form-control" id="member_password" name="member_password" required="required" placeholder="Must be above 8 characters">

                            </div>

                        </div>

                        </div>
    </fieldset>
 
    <h3>Secondary Information</h3>
    <fieldset>
        <legend>Profile Information</legend>
 
        <label for="name-2">First name *</label>
        <input id="name-2" name="name" type="text" class="form-control">
        <label for="surname-2">Last name *</label>
        <input id="surname-2" name="surname" type="text" class="form-control">
        <label for="email-2">Email *</label>
        <input id="email-2" name="email" type="text" class="form-control email">
        <label for="address-2">Address</label>
        <input id="address-2" name="address" type="text">
        <label for="age-2">Age (The warning step will show up if age is less than 18) *</label>
        <input id="age-2" name="age" type="text" class="form-control" >
        <p>(*) Mandatory</p>
    </fieldset>
 
    <h3>Warning</h3>
    <fieldset>
        <legend>You are to young</legend>
 
        <p>Please go away ;-)</p>
    </fieldset>
 
    <h3>Finish</h3>
    <fieldset>
        <legend>Terms and Conditions</legend>
 
        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="form-control"> <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
    </fieldset>
</form>

@endsection
@section('custom_scripts')
<script src="{{asset('assets/jquery.steps/jquery.steps.js')}}"></script>

<script>
var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        return true;
        // Allways allow previous action even if the current form is not valid!
        // if (currentIndex > newIndex)
        // {
        //     return true;
        // }
        // // Forbid next action on "Warning" step if the user is to young
        // if (newIndex === 3 && Number($("#age-2").val()) < 18)
        // {
        //     return false;
        // }
        // // Needed in some cases if the user went back (clean up)
        // if (currentIndex < newIndex)
        // {
        //     // To remove error styles
        //     form.find(".body:eq(" + newIndex + ") label.error").remove();
        //     form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        // }
        // form.validate().settings.ignore = ":enabled,:hidden";
        // return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
    },
    onFinishing: function (event, currentIndex)
    {
        // form.validate().settings.ignore = ":disabled";
        // return form.valid();
        return true;
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
})
$('a[href="#next"]').addClass('btn btn-block btn-success btn-lg');

</script>
@endsection