<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;

class MemberController extends Controller
{
    public function addNewMember(){
        //code here
        return view('assembly.addnewmember');
    }

    public function memberList(){
        return view('assembly.memberlist');
    }
    public function profile($id){
        //code here
        return view('assembly.profile');
    }
    public function saveMemberInformation(Request $r){
        //code here
       Session::flash('success','Member Added Successfully'); 
       return back();
    }
    public function deleteMember($id){
        $del = Member::find($id)->delete();
        if($del){
            return ['status'=>'success'];
        }else{
            return ['status'=>'failed'];
        }
    }

    public function editMember($id){
        $member = Member::find($id);
        return view('assembly.editmember', compact('member'));
    }
    
    public function saveMemberEditInformation(){


    }
}
