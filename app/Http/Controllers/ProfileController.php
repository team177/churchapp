<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function memberPro(){
        return view('profiles.create_member_profile');
    }
    public function editProfile(){
        return view('profiles.editProfile');
    }
}
