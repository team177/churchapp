<?php

namespace App\Http\Controllers;
use App\Member;
use Session;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function login(){
        return view('login');
    }
    public function loginUser(Request $request){
        //return $request->all();
        // $this->validate($request,[
        //     'email'      => 'required|email',
        //     'password'   => 'required|alphaNum|min:3'
        // ]);
        // $user_data = array(
        //     'email' => $request->get('email'),
        //     'password' => $request->get('password')
        // );
        // if(Auth::attempt($user_data)){
            
        //     Session::flash('success', 'Logged in Successfully');
        //     return redirect('/addnewmember');
        // }else{
        //         echo 'hello';
        // }
       
        
        $check = Member::where('email',$request->email)->first();
        if(!is_null($check)){
            if(Hash::check($request->password,$check->password)){
                return redirect('/addnewmember');  
               //echo "login success";
            }else{
                Session::flash('error','Invalid Username or Password');
                return back();
               // echo 'password error';
            }
        }else{
            Session::flash('error','Invalid Username or Password');
            return back();
            //echo 'No data';
        }


    }
    

    public function register(){
        return view('register');
    }
    public function memProfile(){
        return view('memberprofile');
    }
    public function resetPass(){
        return view('reset');
    }

    public function registerUser(Request $request){

        $adduser = Member::create($request->all());
        Session::flash('created', 'Your Account is created successfully');
        return redirect('/login');
    }
    
}
