<?php

namespace App;
use Hash;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
            'user_id',
            'password',
            'firstname',
            'middlename',
            'lastname',
            'nationality',
            'place_of_birth',
            'region_of_birth',
            'hometown',
            'language',
            'occupation',
            'title',
            'gender',
            'dob',
            'date_join',
            'email',
            'postal_add',
            'digi_add',
            'residential_add',
            'phone',
            'emerg_name',
            'emerg_phone',
            'emerg_email',
            'baptism',
            'baptism_date',
            'holy_baptism',
            'holy_baptism_date',
            'mar_status',
            'qualification',
            'other_qualification',
            'profession',
            'image',
            'account_status',
            'extra_info'
    ];
    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

}
